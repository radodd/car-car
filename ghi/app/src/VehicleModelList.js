import { useState, useEffect } from "react";

function VehicleModelList() {
  const [models, setModels] = useState([]);
  const [currentManufacturer, setCurrentManufacturer] = useState(null);

  const fetchData = async () => {
    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = async (event) => {
    const automobilesUrl = `http://localhost:8100/api/models/${event}/`;
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(automobilesUrl, fetchConfig);
    if (response.ok) {
      console.log(response.deleted, response.breakdown);
      fetchData();
    }
  };

  const sortModelsByManufacturer = () => {
    const sortedModels = [...models].sort((a, b) =>
      a.manufacturer.name.localeCompare(b.manufacturer.name)
    );
    return sortedModels;
  };

  //   const renderManufacturerHeader = (manufacturerName) => {
  //     setCurrentManufacturer(manufacturerName);
  //     return (
  //       <div key={manufacturerName} className="w-100">
  //         <h2>{manufacturerName}</h2>
  //       </div>
  //     );
  //   };

  useEffect(() => {
    if (models.length > 0) {
      let isFirst = true;
      for (let i = 0; i < models.length - 1; i++) {
        const manufacturerName = models[i].manufacturer.name;
        const nextManufacturerName = models[i + 1].manufacturer.name;

        if (manufacturerName !== nextManufacturerName) {
          if (isFirst) {
            isFirst = false;
          } else {
            // Add an empty row element between manufacturers
            setCurrentManufacturer(manufacturerName);
          }
        }
      }
    }
  }, [models]);

  //   useEffect(() => {
  //     if (models.length > 0) {
  //       setCurrentManufacturer(models[0].manufacturer.name);
  //     }
  //   }, [models]);

  return (
    <>
      <h1>Vehicle Models</h1>
      <div className="row">
        {sortModelsByManufacturer().map((model) => {
          const manufacturerName = model.manufacturer.name;
          //   if (manufacturerName !== currentManufacturer) {
          //     setCurrentManufacturer(manufacturerName);
          //     return (
          //       <div key={manufacturerName} className="w-100">
          //         <h2>{manufacturerName}</h2>
          //       </div>
          //     );
          //   }
          return (
            <div
              key={model.id}
              className="col-lg-3 col-md-4 col-sm-6 mb-4" // Adjust column size as needed
            >
              <div className="w-100">
                <h2>{manufacturerName}</h2>
              </div>
              {manufacturerName !== currentManufacturer && (
                <div className="row"></div>
              )}

              <div className="card shadow" style={{ width: "250px" }}>
                <img
                  src={model.picture_url}
                  style={{ width: "250px", height: "200px" }}
                  alt={model.name}
                  className="card-img-top"
                />
                <div className="card-body">
                  <label htmlFor="model_name">Model Name</label>
                  <h5 className="card-title">{model.name}</h5>
                  <label htmlFor="manufacturer">Manufacturer</label>
                  <h6 className="card-text text-muted">
                    {model.manufacturer.name}
                  </h6>
                  <button
                    onClick={() => handleDelete(model.id)}
                    className="btn btn-danger"
                  >
                    Delete
                  </button>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
}

export default VehicleModelList;
