import { NavLink } from "react-router-dom";
import React, { useState } from "react";
import "./Nav.css";

function Nav() {
  const [isNavOpen, setIsNavOpen] = useState(false);
  const handleToggleNav = () => {
    setIsNavOpen(!isNavOpen);
  };
  const bodyClass = isNavOpen ? "navbar-open" : "";

  return (
    <nav
      className={`navbar bg-body-tertiary navbar-dark ${
        isNavOpen ? "nav-open" : ""
      }`}
    >
      <div className="container-fluid d-flex">
        <NavLink className="navbar-brand" to="/">
          AutoVantage
        </NavLink>
        <button
          className={`navbar-toggler ${isNavOpen ? "collapsed" : ""}`}
          type="button"
          onClick={handleToggleNav}
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded={isNavOpen}
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className={`collapse navbar-collapse ${isNavOpen ? "open" : ""}`}
          id="navbarSupportedContent"
        >
          <ul className="navbar-nav me-auto mb-2 mb-lg-0 row">
            <div className="col-lg-6">
              <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/">
                  Home
                </NavLink>
              </li>
              <div className="w-100 text-center">
                <hr style={{ border: "2px solid #000" }} />
              </div>
              <li className="nav-link active">Inventory</li>
              <li className="nav-item dropdown">
                <div
                  className="nav-link dropdown-toggle"
                  type="button"
                  id="navbarDropdown"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Manufacturer Actions
                </div>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <NavLink className="dropdown-item" to="manufacturers">
                    Manufacturer List
                  </NavLink>
                  <NavLink className="dropdown-item" to="manufacturer/new">
                    Create Manufacturer
                  </NavLink>
                </div>
              </li>
              <li className="nav-item dropdown">
                <div
                  className="nav-link dropdown-toggle"
                  type="button"
                  id="navbarDropdown"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Vehicle Actions
                </div>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <NavLink className="dropdown-item" to="vehicles">
                    Vehicle List
                  </NavLink>
                  <NavLink className="dropdown-item" to="vehicles/new">
                    Create Vehicle Model
                  </NavLink>
                </div>
              </li>
              <li className="nav-item dropdown">
                <div
                  className="nav-link dropdown-toggle"
                  type="button"
                  id="navbarDropdown"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Automobile Actions
                </div>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <NavLink className="dropdown-item" to="automobiles">
                    Automobile List
                  </NavLink>
                  <NavLink className="dropdown-item" to="automobile/new">
                    Create an Automobile
                  </NavLink>
                </div>
              </li>
              <div className="w-100 text-center">
                <hr style={{ border: "2px solid #000" }} />
              </div>
              <li className="nav-link active">Sales Department</li>
              <li className="nav-item dropdown">
                <div
                  className="nav-link dropdown-toggle"
                  type="button"
                  id="navbarDropdown"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Salespeople Actions
                </div>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <NavLink className="dropdown-item" to="salespeople">
                    Salespeople List
                  </NavLink>
                  <NavLink className="dropdown-item" to="salespeople/new">
                    Create a Salesperson
                  </NavLink>
                </div>
              </li>
              <li className="nav-item dropdown">
                <div
                  className="nav-link dropdown-toggle"
                  type="button"
                  id="navbarDropdown"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Customer Actions
                </div>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <NavLink className="dropdown-item" to="customers">
                    Customer List
                  </NavLink>
                  <NavLink className="dropdown-item" to="customers/new">
                    Create a Customer
                  </NavLink>
                </div>
              </li>
              <li className="nav-item dropdown">
                <div
                  className="nav-link dropdown-toggle"
                  type="button"
                  id="navbarDropdown"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Sales Actions
                </div>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <NavLink className="dropdown-item" to="sales">
                    List of Sales
                  </NavLink>
                  <NavLink className="dropdown-item" to="sales/new">
                    Record a Sale
                  </NavLink>
                  <NavLink className="dropdown-item" to="sales/history">
                    Salesperson History
                  </NavLink>
                </div>
              </li>
              <div className="w-100 text-center">
                <hr style={{ border: "2px solid #000" }} />
              </div>
              <li className="nav-link active">Services Department</li>
              <li className="nav-item dropdown">
                <div
                  className="nav-link dropdown-toggle"
                  type="button"
                  id="navbarDropdown"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Technicians Actions
                </div>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <NavLink className="dropdown-item" to="technicians">
                    Technician List
                  </NavLink>
                  <NavLink className="dropdown-item" to="technician/new">
                    Create a Technician
                  </NavLink>
                </div>
              </li>
              <li className="nav-item dropdown">
                <div
                  className="nav-link dropdown-toggle"
                  type="button"
                  id="navbarDropdown"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Appointment Actions
                </div>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <NavLink className="dropdown-item" to="appointments">
                    List of Appointments
                  </NavLink>
                  <NavLink className="dropdown-item" to="appointment/new">
                    Create an Appointment
                  </NavLink>
                  <NavLink className="dropdown-item" to="servicehistory">
                    Service History
                  </NavLink>
                </div>
              </li>
            </div>
          </ul>
          <div className={`main-content ${bodyClass}`}></div>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
