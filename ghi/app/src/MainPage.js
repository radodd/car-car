import React from "react";
import { NavLink } from "react-router-dom";
import MainPageCar from "./images/MainPageCar.jpg";
import MainPageCar2 from "./images/MainPageCar2.jpg";
import CardImage1 from "./images/CardImage1.jpg";
import CardImage2 from "./images/CardImage2.jpg";
import CardImage3 from "./images/CardImage3.jpg";
import "./MainPage.css";

function MainPage() {
  return (
    <div>
      <div className="text-center">
        <div
          id="carouselHUD"
          className="carousel slide"
          data-bs-ride="carousel"
        >
          <div className="carousel-indicators">
            <button
              type="button"
              data-bs-target="#carouselHUD"
              data-bs-slide-to="0"
              className="active"
              aria-current="true"
              aria-label="Slide 1"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselHUD"
              data-bs-slide-to="1"
              aria-label="Slide 2"
            ></button>
          </div>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img src={MainPageCar} className="d-block" alt="Black car" />
              <div className="carousel-caption">
                <h5 className="logoCarousel">AutoVantage</h5>
                <p className="lead mb-4">
                  The premier solution for automobile dealership management!
                </p>
              </div>
              <div className="faded-overlay"></div>
            </div>
            <div className="carousel-item">
              <img src={MainPageCar2} className="d-block" alt="Black car" />
              <div className="carousel-caption display-5 fw-bold">
                <h5 className="logoCarousel">AutoVantage</h5>
                <p className="lead mb-4">
                  The premier solution for automobile dealership management!
                </p>
              </div>
              <div className="faded-overlay"></div>
            </div>
          </div>
          <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="carouselHUD"
            data-bs-slide="prev"
          >
            <span
              className="carousel-control-prev-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button
            className="carousel-control-next"
            type="button"
            data-bs-target="carouselHUD"
            data-bs-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      </div>
      <div className="container" id="customCards">
        <div className="row">
          <div className="col-md-4">
            <div className="card">
              <img src={CardImage1} className="card-img-top" alt="calendar" />
              <div className="card-body">
                <h5 className="card-title">Schedule an Appointment</h5>
                <p className="card-text">
                  Make a service appointment for your vehicle today.
                </p>
                <NavLink
                  className="btn btn-warning"
                  to="appointment/new"
                  role="button"
                >
                  Make an Appointment
                </NavLink>
              </div>
            </div>
          </div>

          <div className="col-md-4">
            <div className="card">
              <img
                src={CardImage2}
                className="card-img-top"
                alt="magnifying glass"
              />
              <div className="card-body">
                <h5 className="card-title">Search for Vehicle</h5>
                <p className="card-text">
                  Find a vehicle's service history with only the vehicle's VIN
                </p>
                <NavLink
                  className="btn btn-warning"
                  to="appointments"
                  role="button"
                >
                  Find My Vehicle
                </NavLink>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="card">
              <img src={CardImage3} className="card-img-top" alt="blue arrow" />
              <div className="card-body">
                <h5 className="card-title">Employee Login Portal</h5>
                <p className="card-text">Employee access only.</p>
                <NavLink className="btn btn-warning" to="/" role="button">
                  Employees Only
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
