import React, { useState } from "react";

function CustomerForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [address, setAddress] = useState("");
  const [confirmationMessage, setConfirmationMessage] = useState("");

  const handleFirstNameChange = (event) => {
    const firstName = event.target.value;
    setFirstName(firstName);
  };
  const handleLastNameChange = (event) => {
    const lastName = event.target.value;
    setLastName(lastName);
  };
  const handlePhoneNumberChange = async (event) => {
    const phoneNumber = event.target.value;
    const formattedNumber = await formatPhoneNumber(phoneNumber);
    setPhoneNumber(formattedNumber);
  };
  const handleAddressChange = (event) => {
    const address = event.target.value;
    setAddress(address);
  };

  const formatPhoneNumber = async (phoneNumber) => {
    const formattedNumber = phoneNumber.replace(/\D/g, "");
    if (formattedNumber.length === 10) {
      return `(${formattedNumber.slice(0, 3)}) ${formattedNumber.slice(
        3,
        6
      )}-${formattedNumber.slice(6)}`;
    }
    return formattedNumber;
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.first_name = firstName;
    data.last_name = lastName;
    data.phone_number = phoneNumber;
    data.address = address;

    const submitUrl = "http://localhost:8090/api/customers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(submitUrl, fetchConfig);
    if (response.ok) {
      const newCustomer = await response.json();
      console.log(newCustomer);

      setFirstName("");
      setLastName("");
      setPhoneNumber("");
      setAddress("");
      setConfirmationMessage("some message");
    }
  };
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Customer</h1>
          <form onSubmit={handleSubmit} id="create-customer-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFirstNameChange}
                placeholder="firstName"
                value={firstName}
                required
                type="text"
                name="firstname"
                id="firstname"
                className="form-control"
              />
              <label htmlFor="fabric">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleLastNameChange}
                placeholder="lastName"
                value={lastName}
                required
                type="text"
                name="lastname"
                id="lastname"
                className="form-control"
              />
              <label htmlFor="fabric">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handlePhoneNumberChange}
                placeholder="phoneNumber"
                value={phoneNumber}
                required
                type="text"
                name="phonenumber"
                id="phonenumber"
                className="form-control"
              />
              <label htmlFor="fabric">Phone Number</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleAddressChange}
                placeholder="address"
                value={address}
                required
                type="text"
                name="address"
                id="address"
                className="form-control"
              />
              <label htmlFor="fabric">Address</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
          {confirmationMessage && (
            <div className="alert alert-success mt-3">
              Successfully added new manufacturer!
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default CustomerForm;
