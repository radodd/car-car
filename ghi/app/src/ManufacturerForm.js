import React, { useState, useEffect } from "react";

function ManufacturerForm() {
  // const [manufacturers, setManufacturers] = useState([]);
  const [name, setName] = useState("");
  const [logo, setLogo] = useState("");
  const [manufacturers, setManufacturers] = useState([]);
  const [confirmationMessage, setConfirmationMessage] = useState("");

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };
  const handleLogoChange = (event) => {
    const value = event.target.value;
    setLogo(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name;
    data.logo = logo;

    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      setName("");
      setLogo("");
      setConfirmationMessage("some message");
    }
  };

  const fetchData = async (dataType) => {
    let url;
    switch (dataType) {
      case "manufacturers":
        url = "http://localhost:8100/api/manufacturers";
        break;
      default:
        return;
    }
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      if (dataType === "manufacturers") {
        setManufacturers(data.manufacturers);
      }
      console.log(data);
    }
  };
  useEffect(() => {
    fetchData("manufacturers");
  });
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleNameChange}
                placeholder="Manufacturer name"
                value={name}
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Manufacturer name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleLogoChange}
                placeholder="Manufacturer logo URL"
                value={logo}
                required
                type="text"
                name="logo"
                id="logo"
                className="form-control"
              />
              <label htmlFor="logo">Manufacturer logo URL</label>
            </div>
            {logo && (
              <div className="mb-3">
                <img
                  src={logo}
                  alt="manufacturer logo"
                  style={{ maxWidth: "100%", maxHeight: "100%" }}
                />
              </div>
            )}
            <button className="btn btn-primary">Create</button>
          </form>
          {confirmationMessage && (
            <div className="alert alert-success mt-3">
              Successfully added new manufacturer!
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
export default ManufacturerForm;
