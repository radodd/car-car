import { useEffect, useState } from "react";
import "./Table.css";

function ManufacturerList() {
  const [manufacturers, setManufacturers] = useState([]);

  const fetchData = async () => {
    const url = "http://localhost:8100/api/manufacturers";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
      console.log(data);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = async (event) => {
    const manufacturerUrl = `http://localhost:8100/api/manufacturers/${event}/`;
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      console.log(response.deleted, response.breakdown);
      fetchData();
    }
  };

  return (
    <>
      <div className="table">
        <table className="table table-striped table-bordered custom-table">
          <thead className="thead">
            <tr>
              <th>Logo</th>
              <th>Manufacturer</th>
              <th> </th>
            </tr>
          </thead>
          <tbody>
            {manufacturers.map((manufacturer) => {
              return (
                <tr key={manufacturer.id}>
                  <td className="logo">
                    <img
                      src={manufacturer.logo}
                      alt={`${manufacturer.name} Logo`}
                      width="100px"
                      height="100px"
                    />
                  </td>
                  <td className="fs-3">{manufacturer.name}</td>

                  <td>
                    <button
                      onClick={() => handleDelete(manufacturer.id)}
                      className="btn btn-danger"
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default ManufacturerList;
