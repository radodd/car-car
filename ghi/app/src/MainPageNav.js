import React from "react";
import { NavLink } from "react-router-dom";
import "./MainPageNav.css";

function Nav() {
  return (
    <nav className="navbar navbar-dark d-flex">
      <NavLink className="navbar-brand" to="/">
        AutoVantage
      </NavLink>

      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item">
          <NavLink
            className="nav-link active"
            aria-current="page"
            to="/"
          ></NavLink>
        </li>
      </ul>

      {/* Move the "User Login" link to the right */}
      <NavLink className="nav-link login" to="/">
        User Login
      </NavLink>

      <div className="main-page-content"></div>
    </nav>
  );
}

export default Nav;
